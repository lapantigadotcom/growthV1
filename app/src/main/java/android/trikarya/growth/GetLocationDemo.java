package android.trikarya.growth;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by Hendry on 10/19/2017.
 */

public class GetLocationDemo {

    private static final String TAG = "Get Location";
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static FusedLocationProviderClient mFusedLocationClient;
    protected static Location mLastLocation;

    private static Context context;
    public GetLocationDemo(Context c) {
        context = c;
    }

    public static void requestLocation(final Context context, final GetLocationCallback callback) {
        final  LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        if (isGPSEnabled && isNetworkEnabled) {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Location Getter");
            progressDialog.setMessage("Mengambil Koordinat, Mohon Ditunggu...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            mFusedLocationClient.getLastLocation()
            .addOnCompleteListener((Activity) context, new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mLastLocation = task.getResult();
                        Log.d(TAG, String.valueOf(mLastLocation.getLatitude()));
                        callback.Done(mLastLocation);
                        progressDialog.dismiss();
                    } else {
                        Log.w(TAG, "getLastLocation:exception", task.getException());
                    }
                }
            });
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Your GPS is disabled. Enable your GPS and try again!")
                .setCancelable(false)
                .setTitle("Gps Status")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}

