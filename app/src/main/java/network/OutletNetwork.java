package network;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by Hendry on 10/17/2017.
 */

public class OutletNetwork extends BaseNetwork{
    public OutletNetwork(Context context) {
        super(context);
    }
    public void createOutlet(JSONObject jsonObject, JsonCallback jsonCallback){
        connectionHandler.MakeConnection(ConnectionHandler.post_method, "setOutlet", jsonObject, jsonCallback,"Processing","Please Wait");
    }
    public void getImage(int kd_outlet,JsonCallback jsonCallback){
        connectionHandler.MakeConnection(ConnectionHandler.get_method, "/getFoto/"+kd_outlet, null, jsonCallback,"Processing","Please Wait");
    }
}
